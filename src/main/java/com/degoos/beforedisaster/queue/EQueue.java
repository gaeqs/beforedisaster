package com.degoos.beforedisaster.queue;

public interface EQueue<E> {

	void add(E elem);

	E front();

	E remove();

	int size();

	boolean isEmpty();
}
