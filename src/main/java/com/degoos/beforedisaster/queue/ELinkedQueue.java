package com.degoos.beforedisaster.queue;

import java.util.NoSuchElementException;

public class ELinkedQueue<E> implements EQueue<E> {

	private Node<E> first, last;
	private int size;

	public ELinkedQueue() {
		first = null;
		size = 0;
	}

	@Override
	public void add(E elem) {
		Node<E> node = new Node<>(elem);

		if (size == 0) {
			first = node;
			last = node;
		} else {
			last.next = node;
			last = node;
		}
		size++;
	}

	@Override
	public E front() {
		if (first == null) throw new NoSuchElementException();
		return first.value;
	}

	@Override
	public E remove() {
		if (first == null) throw new NoSuchElementException();
		E value = first.value;
		first = first.next;
		size--;
		if (size == 0) last = null;
		return value;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}


	private static class Node<E> {
		E value;
		Node<E> next;

		Node(E v) {
			value = v;
			next = null;
		}
	}
}
