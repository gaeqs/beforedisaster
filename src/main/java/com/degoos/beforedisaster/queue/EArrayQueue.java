package com.degoos.beforedisaster.queue;

import java.util.NoSuchElementException;

public class EArrayQueue<E> implements EQueue<E> {

	private E[] array;
	private int size;

	private int front, last;

	public EArrayQueue(int arraySize) {
		array = (E[]) new Object[arraySize];
		size = 0;
		front = 0;
		last = 0;
	}

	@Override
	public void add(E elem) {
		if (size == array.length) throw new IllegalStateException();
		array[last] = elem;
		last++;
		size++;

		if (last == array.length) last = 0;
	}

	@Override
	public E front() {
		if (size == 0) throw new NoSuchElementException();
		return array[front];
	}

	@Override
	public E remove() {
		if (size == 0) throw new NoSuchElementException();
		E elem = array[front];
		array[front] = null;
		size--;
		front++;
		if (front == array.length) front = 0;
		return elem;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}
}
