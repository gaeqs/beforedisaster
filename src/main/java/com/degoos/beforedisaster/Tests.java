package com.degoos.beforedisaster;

import com.degoos.beforedisaster.list.ELinkedList;
import com.degoos.beforedisaster.list.EList;
import com.degoos.beforedisaster.queue.ELinkedQueue;
import com.degoos.beforedisaster.queue.EQueue;
import com.degoos.beforedisaster.set.EArraySet;
import com.degoos.beforedisaster.set.ESet;
import com.degoos.beforedisaster.stack.EStack;
import com.degoos.beforedisaster.tree.EAnaBinaryTree;

import java.util.List;

public class Tests {

	public static void main(String[] args) {

		ESet<String> set = new EArraySet<>();
		System.out.println(set);
		set.add("a");
		set.add("b");
		set.add("c");
		System.out.println(set);
		set.add("b");
		System.out.println(set);
		set.add("d");
		System.out.println(set);
		set.remove("b");
		System.out.println(set);
		set.add("b");
		System.out.println(set);

		ESet<String> uwu = new EArraySet<>();
		uwu.add("a");
		uwu.add("uwu");
		uwu.add("owo");
		uwu.add("3");

		set.removeExcept(uwu);
		System.out.println(set);
	}

	public static boolean sorted(EAnaBinaryTree<Integer> tree) {
		if (tree.isEmpty() || tree.isLeaf()) return true;


		EQueue<EAnaBinaryTree<Integer>> queue = new ELinkedQueue<>();
		int lower = tree.getValue();
		boolean sorted = true;

		if (tree.hasLeft()) queue.add(tree.getLeft());
		if (tree.hasRight()) queue.add(tree.getRight());

		EAnaBinaryTree<Integer> current;
		while (sorted && queue.isEmpty()) {
			current = queue.remove();
			if (lower <= current.getValue()) {
				lower = current.getValue();
				if (current.hasLeft()) queue.add(current.getLeft());
				if (current.hasRight()) queue.add(current.getRight());
			} else {
				sorted = false;
			}
		}

		return sorted;
	}

	public static EList<Integer> sortedList(EList<Integer> list1, EList<Integer> list2, EStack<Integer> stack) {
		if (list1.isEmpty() && list2.isEmpty()) return new ELinkedList<>();
		EList<Integer> finalList = new ELinkedList<>();

		int min;

		if (list1.isEmpty()) min = list2.get(0);
		else if (list2.isEmpty()) min = list1.get(0);
		else min = Math.min(list1.get(0), list2.get(0));

		int max;

		if (list1.isEmpty()) max = list2.get(list2.size() - 1);
		else if (list2.isEmpty()) max = list1.get(list1.size() - 1);
		else max = Math.max(list1.get(list1.size() - 1), list2.get(list2.size() - 1));

		for (int i = min; i <= max; i++) {
			if (list1.contains(i)) {
				finalList.add(i);
				if (list2.contains(i)) stack.push(i);
			} else if (list2.contains(i))
				finalList.add(i);
		}

		return finalList;
	}


	public static int getTreeQueueSize(EAnaBinaryTree<Integer> tree) {
		if (tree.isEmpty()) return 0;
		if (tree.isLeaf()) return 1;
		return getTreeQueueSize(tree.getLeft()) + getTreeQueueSize(tree.getRight());

	}

	public static EList<Integer> sorted(List<Integer> list1, List<Integer> list2, EStack<Integer> stack) {
		EList<Integer> finalList = new ELinkedList<>();
		if (list1.isEmpty() && list2.isEmpty()) return finalList;

		int min;
		int max;
		if (list1.isEmpty()) {
			min = list2.get(0);
			max = list2.get(list2.size() - 1);
		} else {
			if (list2.isEmpty()) {
				min = list1.get(0);
				max = list1.get(list1.size() - 1);
			} else {
				min = Math.min(list1.get(0), list2.get(0));
				max = Math.max(list1.get(list1.size() - 1), list2.get(list2.size() - 1));
			}
		}

		for (int i = min; i <= max; i++) {
			if (list1.contains(i)) {
				finalList.add(i);
				if (list2.contains(i))
					stack.push(i);
			} else if (list2.contains(i))
				finalList.add(i);
		}
		return finalList;
	}

}
