package com.degoos.beforedisaster.list;

import com.degoos.beforedisaster.tree.ESimpleLinkedTree;

public class ESimpleLinkedList /*implements Lista*/ {

	private int size;
	private Node first;

	public ESimpleLinkedList() {
		size = 0;
		first = null;
	}

	public boolean removeElem(int elem) {
		if (size == 0) return false;
		Node current = first;
		Node previous = null;
		while (current != null) {
			if (current.value == elem) {
				if (previous == null) {
					first = first.next;
				} else {
					previous.next = current.next;

				}
				size--;
				return true;

			}
			previous = current;
			current = current.next;

		}
		return false;

	}

	public boolean equals(ESimpleLinkedList list) {
		if (list == this) return true;
		if (list == null) return false;
		if (size != list.size) return false;
		Node current = first;
		Node current2 = list.first;
		while (current.value == current2.value) {
			if (current.next == null) return true;
			current = current.next;
			current2 = current2.next;
		}
		return false;


	}
	public void add(int index, int elem) throws IndexOutOfBoundsException{
		if(index<0||index>size)throw new IndexOutOfBoundsException();
		Node node=new Node(elem);
		if (size==0){
			first=node;
			size++;
			System.out.println("Ole tu polla node que eres el unico.");
			return;
		}
		if (index==0){
			node.next=first;
			first=node;
			size++;
			return;
		}
		Node current=first;
		for(int i=0; i<index-1;i++){
			current=current.next;

		}
		node.next=current.next.next;
		current.next=node;
		size++;



	}


	public class Node {
		int value;
		Node next;

		public Node(int value) {
			this.value = value;
			next = null;


		}


	}


}
