package com.degoos.beforedisaster.list;

import com.degoos.beforedisaster.EIterator;

public class EArrayList<E> implements EList<E> {

	private E[] array;
	private int size;

	public EArrayList() {
		array = (E[]) new Object[10];
		size = 0;
	}

	@Override
	public boolean add(E elem) {
		boolean expand = false;
		if (size == array.length) {
			expandArray();
			expand = true;
		}
		array[size] = elem;
		size++;
		return expand;
	}

	@Override
	public boolean remove(E elem) {
		int index = 0;
		boolean found = false;
		E current;
		while (index < size && !found) {
			current = array[index];
			if (current.equals(elem)) {
				removeElem(index);
				found = true;
			}
			index++;
		}
		return found;
	}

	@Override
	public boolean contains(E elem) {
		int index = 0;
		boolean found = false;
		E current;
		while (index < size && !found) {
			current = array[index];
			if (current.equals(elem))
				found = true;
			index++;
		}
		return found;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void clear() {
		for (int i = 0; i < size; i++) {
			array[i] = null;
		}
		size = 0;
	}

	@Override
	public EIterator<E> iterator() {
		return new EArrayIterator();
	}

	@Override
	public boolean add(int index, E elem) {
		if (index < 0 || index > size) throw new IndexOutOfBoundsException();
		if (index == size) return add(elem);
		boolean expand = false;
		if (array.length == size) {
			expandArray();
			expand = true;
		}
		for (int i = size - 1; i >= index; i--) {
			array[i + 1] = array[i];
		}
		array[index] = elem;
		size++;

		return expand;
	}

	@Override
	public E get(int index) {
		return array[index];
	}

	@Override
	public E set(int index, E elem) {
		if (index < 0 || index >= size) throw new IndexOutOfBoundsException();
		E previous = array[index];
		array[index] = elem;
		return previous;
	}

	@Override
	public E removeElem(int index) {
		if (index < 0 || index >= size) throw new IndexOutOfBoundsException();
		E elem = array[index];
		for (int i = index; i < size - 1; i++) {
			array[i] = array[i + 1];
		}
		array[size - 1] = null;
		size--;
		return elem;
	}

	private void expandArray() {
		E[] newArray = (E[]) new Object[array.length * 2];
		for (int i = 0; i < size; i++) {
			newArray[i] = array[i];
		}
		array = newArray;
	}

	@Override
	public String toString() {
		if (size == 0) return "{}";
		String s = "{";
		for (int i = 0; i < size - 1; i++) {
			s += array[i] + ", ";
		}
		s += array[size - 1] + "}";
		return s;
	}


	private class EArrayIterator implements EIterator<E> {

		private int current;

		public EArrayIterator() {
			current = 0;
		}

		@Override
		public boolean hasNext() {
			return current < size;
		}

		@Override
		public E next() {
			E elem = array[current];
			current++;
			return elem;
		}
	}

}
