package com.degoos.beforedisaster.list;

import com.degoos.beforedisaster.EIterator;

public interface EList<E> {

	boolean add(E elem);

	boolean remove(E elem);

	boolean contains(E elem);

	int size();

	boolean isEmpty();

	void clear();

	EIterator<E> iterator();

	//LIST

	boolean add(int index, E elem);

	E get(int index);

	E set(int index, E elem);

	E removeElem(int index);

}
