package com.degoos.beforedisaster.list;

import com.degoos.beforedisaster.EIterator;

public class EDoubleLinkedList<E> implements EList<E> {

	private Node<E> first, last;


	public EDoubleLinkedList() {
	}

	@Override
	public boolean add(E elem) {
		Node<E> node = new Node<>(elem);
		if (first == null) {
			first = node;
			last = node;
		} else {
			node.previous = last;
			last.next = node;
			last = node;
		}
		return true;
	}

	@Override
	public boolean remove(E elem) {
		Node<E> current = first;
		boolean found = false;
		while (!found && current != null) {
			if (current.value.equals(elem)) {
				found = true;

				if (first == last) {
					first = null;
					last = null;
				} else if (current == first) {
					first = first.next;
					first.previous = null;
				} else if (current == last) {
					last = last.previous;
					last.next = null;
				} else {
					current.previous.next = current.next;
					current.next.previous = current.previous;
				}
			} else current = current.next;
		}
		return found;
	}

	@Override
	public boolean contains(E elem) {
		Node<E> current = first;
		boolean found = false;
		while (!found && current != null) {
			if (current.value.equals(elem)) found = true;
			else current = current.next;
		}
		return found;
	}

	@Override
	public int size() {
		int size = 0;
		Node<E> current = first;
		while (current != null) {
			size++;
			current = current.next;
		}
		return size;
	}

	@Override
	public boolean isEmpty() {
		return first == null;
	}

	@Override
	public void clear() {
		first = null;
		last = null;
	}

	@Override
	public EIterator<E> iterator() {
		return null;
	}

	@Override
	public boolean add(int index, E elem) {
		int size = size();
		if (index < 0 || index > size) throw new IndexOutOfBoundsException();
		if (size == index) return add(elem);

		Node<E> node = new Node<>(elem);

		if (index == 0) {
			node.next = first;
			first.previous = node;
			first = node;
		} else {
			Node<E> current = getNode(index);

			node.next = current;
			node.previous = current.previous;
			current.previous.next = node;
			current.previous = node;
		}

		return true;
	}

	@Override
	public E get(int index) {
		if (index < 0 || index >= size()) throw new IndexOutOfBoundsException();
		return getNode(index).value;
	}

	@Override
	public E set(int index, E elem) {
		if (index < 0 || index >= size()) throw new IndexOutOfBoundsException();
		Node<E> current = getNode(index);
		E previous = current.value;
		current.value = elem;
		return previous;
	}

	@Override
	public E removeElem(int index) {
		int size = size();
		if (index < 0 || index >= size) throw new IndexOutOfBoundsException();
		E elem;

		if (size == 1) {
			elem = first.value;
			first = null;
			last = null;
		} else if (index == 0) {
			elem = first.value;
			first = first.next;
			first.previous = null;
		} else if (index == size - 1) {
			elem = last.value;
			last = last.previous;
			last.next = null;
		} else {
			Node<E> node = getNode(index);
			elem = node.value;
			node.previous.next = node.next;
			node.next.previous = node.previous;
		}

		return elem;
	}

	public E removeLast() {
		E elem = last.value;
		last.previous.next = null;
		last = last.previous;
		return elem;
	}

	public void addFirst(E elem) {
		Node<E> node = new Node<>(elem);
		node.next = first;
		first.previous = node;
		first = node;
	}

	private Node<E> getNode(int index) {
		Node<E> current = first;
		for (int i = 0; i < index; i++)
			current = current.next;
		return current;
	}

	@Override
	public String toString() {
		if (first == null) return "{}";

		Node<E> node = first;
		String string = "{";
		while (node.next != null) {
			string += node.value + ", ";
			node = node.next;
		}
		return string + node.value + "}";
	}

	private static class Node<E> {

		private E value;
		private Node<E> previous, next;


		private Node(E v) {
			value = v;
		}

	}
}
