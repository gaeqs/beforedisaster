package com.degoos.beforedisaster.list;

import com.degoos.beforedisaster.EIterator;

public class ELinkedList<E> implements EList<E> {

	private Node<E> first, last;
	private int size;

	public ELinkedList() {
		first = null;
		last = null;
		size = 0;
	}

	@Override
	public boolean add(E elem) {
		Node<E> newNode = new Node<>(elem);
		if (size == 0) {
			first = newNode;
			last = newNode;
		} else {
			last.next = newNode;
			last = newNode;
		}
		size++;
		return true;
	}

	@Override
	public boolean remove(E elem) {
		Node<E> previous = null;
		Node<E> current = first;
		boolean found = false;
		while (current != null && !found) {
			if (current.value.equals(elem)) {

				if (previous == null) {
					if (size == 1) {
						first = null;
						last = null;
					} else
						first = first.next;
				} else if (current == last) {
					last = previous;
					previous.next = null;
				} else {
					previous.next = previous.next.next;
				}
				size--;
				found = true;
			}


			previous = current;
			current = current.next;
		}
		return found;
	}

	@Override
	public boolean contains(E elem) {
		Node<E> current = first;
		boolean found = false;
		while (current != null && !found) {
			if (current.value.equals(elem)) {
				found = true;
			}
			current = current.next;
		}
		return found;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void clear() {
		first = null;
		last = null;
		size = 0;
	}

	@Override
	public EIterator<E> iterator() {
		return null;
	}

	@Override
	public boolean add(int index, E elem) {
		return false;
	}

	@Override
	public E get(int index) {
		if (index < 0 || index >= size) throw new IndexOutOfBoundsException();
		return getNode(index).value;
	}

	@Override
	public E set(int index, E elem) {
		if (index < 0 || index >= size) throw new IndexOutOfBoundsException();
		Node<E> node = getNode(index);
		E previous = node.value;
		node.value = elem;
		return previous;
	}

	@Override
	public E removeElem(int index) {
		if (index < 0 || index >= size) throw new IndexOutOfBoundsException();
		E elem;
		if (index == 0) {
			elem = first.value;
			if (size == 1) {
				first = null;
				last = null;
			} else first = first.next;
		} else if (index == size - 1) {
			elem = last.value;
			Node<E> previous = getNode(size - 2);
			previous.next = null;
			last = previous;
		} else {
			Node<E> previous = getNode(index - 1);
			elem = previous.next.value;
			previous.next = previous.next.next;
		}
		size--;
		return elem;
	}

	private Node<E> getNode(int index) {
		if (index == size - 1) return last;
		int current = 0;
		Node<E> node = first;
		while (current < index) {
			node = node.next;
			current++;
		}
		return node;
	}

	@Override
	public String toString() {
		if (first == null) return "{}";

		Node<E> node = first;
		String string = "{";
		while (node.next != null) {
			string += node.value + ", ";
			node = node.next;
		}
		return string + node.value + "}";
	}

	private static class Node<E> {

		E value;
		Node<E> next;

		Node(E v) {
			value = v;
			next = null;
		}

	}
}
