package com.degoos.beforedisaster.stack;

import java.util.NoSuchElementException;

public class ELinkedStack<E> implements EStack<E> {

	private Node<E> first;
	private int size;

	public ELinkedStack() {
		first = null;
		size = 0;
	}

	@Override
	public void push(E elem) {
		Node<E> node = new Node<>(elem);
		node.next = first;
		first = node;
		size++;
	}

	@Override
	public E peek() {
		if (first == null) throw new NoSuchElementException();
		return first.value;
	}

	@Override
	public E pop() {
		if (first == null) throw new NoSuchElementException();
		E value = first.value;
		first = first.next;
		size--;
		return value;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public String toString() {
		if (first == null) return "{}";

		Node<E> node = first;
		String string = "{";
		while (node.next != null) {
			string += node.value + ", ";
			node = node.next;
		}
		return string + node.value + "}";
	}

	private static class Node<E> {
		E value;
		Node<E> next;

		Node(E v) {
			value = v;
			next = null;
		}
	}
}
