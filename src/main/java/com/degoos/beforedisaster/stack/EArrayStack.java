package com.degoos.beforedisaster.stack;

import java.util.NoSuchElementException;

public class EArrayStack<E> implements EStack<E> {

	private E[] array;
	private int size;

	public EArrayStack() {
		array = (E[]) new Object[10];
		size = 0;
	}

	@Override
	public void push(E elem) {
		if (size == array.length) expand();
		array[size] = elem;
		size++;
	}

	@Override
	public E peek() {
		if (size == 0) throw new NoSuchElementException();
		return array[size - 1];
	}

	@Override
	public E pop() {
		if (size == 0) throw new NoSuchElementException();
		E elem = array[size - 1];
		array[size - 1] = null;
		size--;
		return elem;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	public void expand() {
		E[] nArray = (E[]) new Object[array.length * 2];
		for (int i = 0; i < size; i++) {
			nArray[i] = array[i];
		}
		array = nArray;
	}
}
