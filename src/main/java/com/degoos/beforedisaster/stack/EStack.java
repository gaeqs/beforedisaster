package com.degoos.beforedisaster.stack;

public interface EStack<E> {

	void push(E elem);

	E peek();

	E pop();

	int size();

	boolean isEmpty();

}
