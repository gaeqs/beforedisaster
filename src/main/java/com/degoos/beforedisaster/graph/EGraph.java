package com.degoos.beforedisaster.graph;

import com.degoos.beforedisaster.list.EArrayList;

public interface EGraph<E> {

	boolean addNode(E node);

	void addArc(E node1, E node2, int weight);

	int size();

	boolean contains(E node);

	boolean containsArc(E node1, E node2);

	boolean deleteNode(E node);

	boolean deleteArc(E node1, E node2);

	boolean replaceNode(E node1, E node2);

	boolean changeArcWeight(E node1, E node2, int weight);

	EArrayList<E> nodes();

	EArrayList<E> adjacents(E node);

	boolean isDirected();

	boolean isTagged();

}
