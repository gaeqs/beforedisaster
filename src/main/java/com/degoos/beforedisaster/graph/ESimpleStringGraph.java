package com.degoos.beforedisaster.graph;

public class ESimpleStringGraph {

	private static final int MAX_NODES = 10;

	private String[] nodes;
	private int matrix[][];
	private int nodeAmount;

	public ESimpleStringGraph() {
		nodes = new String[MAX_NODES];
		matrix = new int[MAX_NODES][MAX_NODES];
		nodeAmount = 0;
	}

	public boolean addNode(String elem) {
		if (nodeAmount == nodes.length)
			throw new IllegalStateException("The graph is full!");
		if (getNodeIndex(elem) != -1) return false;
		nodes[nodeAmount] = elem;
		nodeAmount++;
		return true;
	}

	public void addArc(String elem1, String elem2) {
		int elem1Index = getNodeIndex(elem1);
		if (elem1Index == -1) throw new IllegalArgumentException(elem1 + " is not a node.");
		int elem2Index = getNodeIndex(elem2);
		if (elem2Index == -1) throw new IllegalArgumentException(elem2 + " is not a node.");
		matrix[elem1Index][elem2Index] = 1;
	}

	public boolean removeNode(String elem) {
		int index = getNodeIndex(elem);
		if (index == -1) return false;
		nodes[index] = null;
		for (int i = 0; i < nodeAmount; i++) {
			matrix[index][i] = 0;
			matrix[i][index] = 0;
		}
		return true;
	}

	private int getNodeIndex(String elem) {
		int current = 0;
		boolean found = false;
		while (!found && current < nodeAmount) {
			if (elem.equals(nodes[current]))
				found = true;
			else current++;
		}
		if (!found) return -1;
		return current;
	}
}
