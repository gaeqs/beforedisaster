package com.degoos.beforedisaster.graph;

import com.degoos.beforedisaster.list.EArrayList;

import java.util.NoSuchElementException;

public class EAMGraph<E> implements EGraph<E> {

	private boolean tagged, directed;

	private E[] nodes;
	private int[][] matrix;
	private int size, lastIndex;

	public EAMGraph(int defaultSize, boolean isTagged, boolean isDirected) {
		tagged = isTagged;
		directed = isDirected;
		nodes = (E[]) new Object[defaultSize];
		matrix = new int[defaultSize][defaultSize];
		size = 0;
		lastIndex = 0;
	}

	@Override
	public boolean addNode(E node) {
		if (contains(node)) return false;
		if (lastIndex == nodes.length) expand();
		nodes[lastIndex] = node;size++;
		lastIndex++;
		return true;
	}

	@Override
	public void addArc(E node1, E node2, int weight) {
		if (weight <= 0) throw new IllegalArgumentException();
		int index1 = getNodeIndex(node1);
		if (index1 == -1) throw new NoSuchElementException();
		int index2 = getNodeIndex(node2);
		if (index2 == -1) throw new NoSuchElementException();


		if (!tagged) {
			matrix[index1][index2] = 1;
		} else {
			matrix[index1][index2] = weight;
		}
		if (!directed) matrix[index2][index1] = matrix[index1][index2];
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean contains(E node) {
		boolean found = false;
		int index = 0;
		E current;
		while (index < lastIndex && !found) {
			current = nodes[index];
			if (node.equals(current)) found = true;
			else index++;
		}
		return found;
	}

	@Override
	public boolean containsArc(E node1, E node2) {
		int index1 = getNodeIndex(node1);
		if (index1 == -1) throw new NoSuchElementException();
		int index2 = getNodeIndex(node2);
		if (index2 == -1) throw new NoSuchElementException();

		return matrix[index1][index2] != 0;
	}

	@Override
	public boolean deleteNode(E node) {
		int index = getNodeIndex(node);
		if (index == -1) return false;

		nodes[index] = null;

		for (int i = 0; i < lastIndex; i++) {
			matrix[index][i] = 0;
			matrix[i][index] = 0;
		}


		size--;


		return true;
	}

	@Override
	public boolean deleteArc(E node1, E node2) {
		int index1 = getNodeIndex(node1);
		if (index1 == -1) throw new NoSuchElementException();
		int index2 = getNodeIndex(node2);
		if (index2 == -1) throw new NoSuchElementException();

		boolean present = matrix[index1][index2] != 0;
		matrix[index1][index2] = 0;
		if (!directed) matrix[index2][index1] = 0;
		return present;
	}

	@Override
	public boolean replaceNode(E node1, E node2) {
		if (contains(node2)) return false;
		int index1 = getNodeIndex(node1);
		if (index1 == -1) return false;
		nodes[index1] = node2;
		return true;
	}

	@Override
	public boolean changeArcWeight(E node1, E node2, int weight) {
		if (weight < 0) throw new IllegalArgumentException();
		int index1 = getNodeIndex(node1);
		if (index1 == -1) throw new NoSuchElementException();
		int index2 = getNodeIndex(node2);
		if (index2 == -1) throw new NoSuchElementException();

		boolean present = matrix[index1][index2] != 0;
		if (present) {
			if (!tagged) {
				if (weight > 0)
					matrix[index1][index2] = 1;
				else matrix[index1][index2] = 0;
			} else {
				matrix[index1][index2] = weight;
			}
			if (!directed) matrix[index2][index1] = matrix[index1][index2];
		}
		return present;
	}

	@Override
	public EArrayList<E> nodes() {
		EArrayList<E> list = new EArrayList<E>();
		for (int i = 0; i < lastIndex; i++) {
			if (nodes[i] != null)
				list.add(nodes[i]);
		}
		return list;
	}

	@Override
	public EArrayList<E> adjacents(E node) {
		EArrayList<E> list = new EArrayList<E>();

		int index = getNodeIndex(node);
		if (index == -1) throw new NoSuchElementException();

		for (int i = 0; i < lastIndex; i++) {
			if (matrix[index][i] != 0) list.add(nodes[i]);
		}
		return list;
	}

	@Override
	public boolean isDirected() {
		return directed;
	}

	@Override
	public boolean isTagged() {
		return tagged;
	}

	private int getNodeIndex(E node) {
		boolean found = false;
		int index = 0;
		E current;
		while (index < lastIndex && !found) {
			current = nodes[index];
			if (node.equals(current)) found = true;
			else index++;
		}
		if (found) return index;
		else return -1;
	}

	private void expand() {
		E[] array = (E[]) new Object[nodes.length * 2];
		for (int i = 0; i < lastIndex; i++) {
			array[i] = nodes[i];
		}
		nodes = array;

		int[][] nMatrix = new int[matrix.length * 2][matrix.length * 2];

		for (int i = 0; i < lastIndex; i++) {
			for (int j = 0; j < lastIndex; j++) {
				nMatrix[i][j] = matrix[i][j];
			}
		}
		matrix = nMatrix;
	}

	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < lastIndex; i++) {
			if (nodes[i] != null) {
				for (int j = 0; j < lastIndex; j++) {
					if (nodes[j] != null)
						s += matrix[i][j] + " ";
				}
				s += "\n";
			}
		}
		return s;
	}
}
