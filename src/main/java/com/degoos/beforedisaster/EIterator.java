package com.degoos.beforedisaster;

public interface EIterator<E> {

	boolean hasNext();

	E next();

}
