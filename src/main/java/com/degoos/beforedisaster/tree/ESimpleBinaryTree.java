package com.degoos.beforedisaster.tree;

import java.util.ArrayList;
import java.util.List;

public class ESimpleBinaryTree<E> implements EBinaryTree<E> {

	private E value;
	private EBinaryTree<E> right, left, parent;

	public ESimpleBinaryTree(E elem) {
		value = elem;
		right = null;
		left = null;
		parent = null;
	}

	private ESimpleBinaryTree(E elem, EBinaryTree<E> par) {
		value = elem;
		right = null;
		left = null;
		parent = par;
	}

	@Override
	public E getValue() {
		return value;
	}

	@Override
	public void setValue(E elem) {
		value = elem;
	}

	@Override
	public EBinaryTree<E> getRight() {
		return right;
	}

	@Override
	public void setRight(E elem) {
		right = new ESimpleBinaryTree<>(elem, this);
	}

	@Override
	public boolean deleteRight() {
		if (right == null) return false;
		right = null;
		return true;
	}

	@Override
	public boolean hasRight() {
		return right != null;
	}

	@Override
	public EBinaryTree<E> getLeft() {
		return left;
	}

	@Override
	public void setLeft(E elem) {
		left = new ESimpleBinaryTree<>(elem, this);
	}

	@Override
	public boolean deleteLeft() {
		if (left == null) return false;
		left = null;
		return true;
	}

	@Override
	public boolean hasLeft() {
		return left != null;
	}

	@Override
	public EBinaryTree<E> getParent() {
		return parent;
	}

	@Override
	public EBinaryTree<E> getRoot() {
		if (parent == null) return this;
		return parent.getRoot();
	}

	@Override
	public EBinaryTree<E> getChildWithValue(E elem) {
		EBinaryTree<E> tree = null;
		if (value.equals(elem)) tree = this;

		if (tree == null && left != null) tree = left.getChildWithValue(elem);
		if (tree == null && right != null) tree = right.getChildWithValue(elem);

		return tree;
	}

	@Override
	public boolean isLeaf() {
		return left == null && right == null;
	}

	@Override
	public List<E> preOrder() {
		ArrayList<E> list = new ArrayList<>();
		preOrder(list);
		return list;
	}

	@Override
	public void preOrder(List<E> list) {
		list.add(value);
		if (left != null)
			left.preOrder(list);
		if (right != null)
			right.preOrder(list);
	}

	@Override
	public List<E> inOrder() {
		ArrayList<E> list = new ArrayList<>();
		inOrder(list);
		return list;
	}

	@Override
	public void inOrder(List<E> list) {
		if (left != null)
			left.inOrder(list);
		list.add(value);
		if (right != null)
			right.inOrder(list);
	}

	@Override
	public List<E> postOrder() {
		ArrayList<E> list = new ArrayList<>();
		postOrder(list);
		return list;
	}

	@Override
	public void postOrder(List<E> list) {
		if (left != null)
			left.postOrder(list);
		if (right != null)
			right.postOrder(list);
		list.add(value);
	}
}
