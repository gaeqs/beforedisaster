package com.degoos.beforedisaster.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class EAnaBinaryTree<E> implements EBinaryTree<E> {

	private EAnaBinaryTreeNode<E> node;

	public EAnaBinaryTree() {
		node = null;
	}

	public EAnaBinaryTree(EAnaBinaryTree<E> a1, E value, EAnaBinaryTree<E> a2) {
		node = new EAnaBinaryTreeNode<>(value);
		node.setLeft(a1);
		node.setRight(a2);

		if (a1.node != null)
			a1.node.setParent(this);
		if (a2.node != null)
			a2.node.setParent(this);
	}

	public EAnaBinaryTree(E value) {
		node = new EAnaBinaryTreeNode<>(value);
	}

	public boolean isEmpty () {
		return node == null;
	}

	@Override
	public E getValue() {
		if (node == null) throw new NoSuchElementException();
		return node.getValue();
	}

	@Override
	public void setValue(E elem) {
		if (node == null) throw new NoSuchElementException();
	}

	@Override
	public EAnaBinaryTree<E> getRight() {
		if (node == null) throw new NoSuchElementException();
		return node.getRight();
	}

	@Override
	public void setRight(E elem) {
		if (node == null) throw new NoSuchElementException();
		if (node.getRight().node != null) throw new UnsupportedOperationException();
		EAnaBinaryTree<E> tree = new EAnaBinaryTree<>(elem);
		node.setRight(tree);
		tree.node.setParent(this);
	}

	@Override
	public boolean deleteRight() {
		if (node == null) throw new NoSuchElementException();
		if (node.getRight().node == null) return false;
		node.setRight(new EAnaBinaryTree<>());
		return true;
	}

	@Override
	public boolean hasRight() {
		if (node == null) throw new NoSuchElementException();
		return node.getRight().node != null;
	}

	@Override
	public EAnaBinaryTree<E> getLeft() {
		if (node == null) throw new NoSuchElementException();
		return node.getLeft();
	}

	@Override
	public void setLeft(E elem) {
		if (node == null) throw new NoSuchElementException();
		if (node.getLeft().node != null) throw new UnsupportedOperationException();
		EAnaBinaryTree<E> tree = new EAnaBinaryTree<>(elem);
		node.setLeft(tree);
		tree.node.setParent(this);
	}

	@Override
	public boolean deleteLeft() {
		if (node == null) throw new NoSuchElementException();
		if (node.getLeft().node == null) return false;
		node.setLeft(new EAnaBinaryTree<>());
		return true;
	}

	@Override
	public boolean hasLeft() {
		if (node == null) throw new NoSuchElementException();
		return node.getLeft().node != null;
	}

	@Override
	public EBinaryTree<E> getParent() {
		if (node == null) throw new NoSuchElementException();
		return node.getParent();
	}

	@Override
	public EBinaryTree<E> getRoot() {
		if (node == null) throw new NoSuchElementException();
		if (node.getParent().node == null)
			return this;
		return node.getParent().getRoot();
	}

	@Override
	public EBinaryTree<E> getChildWithValue(E elem) {
		if (node == null) throw new NoSuchElementException();
		EBinaryTree<E> tree = null;
		if (node.getValue().equals(elem)) tree = this;
		if (tree == null && node.getRight().node != null) tree = node.getRight().getChildWithValue(elem);
		if (tree == null && node.getLeft().node != null) tree = node.getLeft().getChildWithValue(elem);
		return tree;
	}

	@Override
	public boolean isLeaf() {
		if (node == null) throw new NoSuchElementException();
		return node.getLeft().node == null && node.getRight().node == null;
	}

	@Override
	public List<E> preOrder() {
		ArrayList<E> list = new ArrayList<>();
		preOrder(list);
		return list;
	}

	@Override
	public void preOrder(List<E> list) {
		if (node == null) throw new NoSuchElementException();
		list.add(node.getValue());
		if (node.getLeft().node != null)
			node.getLeft().preOrder(list);
		if (node.getRight().node != null)
			node.getRight().preOrder(list);
	}

	@Override
	public List<E> inOrder() {
		ArrayList<E> list = new ArrayList<>();
		inOrder(list);
		return list;
	}

	@Override
	public void inOrder(List<E> list) {
		if (node == null) throw new NoSuchElementException();
		if (node.getLeft().node != null)
			node.getLeft().preOrder(list);
		list.add(node.getValue());
		if (node.getRight().node != null)
			node.getRight().preOrder(list);
	}

	@Override
	public List<E> postOrder() {
		ArrayList<E> list = new ArrayList<>();
		postOrder(list);
		return list;
	}

	@Override
	public void postOrder(List<E> list) {
		if (node == null) throw new NoSuchElementException();
		if (node.getLeft().node != null)
			node.getLeft().preOrder(list);
		if (node.getRight().node != null)
			node.getRight().preOrder(list);
		list.add(node.getValue());
	}
}
