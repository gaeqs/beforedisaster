package com.degoos.beforedisaster.tree;

import java.util.List;

public interface EBinaryTree<E> {

	E getValue();

	void setValue(E elem);

	EBinaryTree<E> getRight();

	void setRight(E elem);

	boolean deleteRight();

	boolean hasRight();

	EBinaryTree<E> getLeft();

	void setLeft(E elem);

	boolean deleteLeft();

	boolean hasLeft();

	EBinaryTree<E> getParent();

	EBinaryTree<E> getRoot();

	EBinaryTree<E> getChildWithValue(E elem);

	boolean isLeaf();

	List<E> preOrder();

	void preOrder(List<E> list);

	List<E> inOrder();

	void inOrder(List<E> list);

	List<E> postOrder();

	void postOrder(List<E> list);

}
