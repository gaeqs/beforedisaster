package com.degoos.beforedisaster.tree;

public class EAnaBinaryTreeNode<E> {

	private E value;
	private EAnaBinaryTree<E> left, right, parent;

	EAnaBinaryTreeNode(E elem) {
		value = elem;
		left = new EAnaBinaryTree<>();
		right = new EAnaBinaryTree<>();
		left = new EAnaBinaryTree<>();
	}


	public E getValue() {
		return value;
	}

	public void setValue(E value) {
		this.value = value;
	}

	public EAnaBinaryTree<E> getRight() {
		return right;
	}

	public void setRight(EAnaBinaryTree<E> right) {
		this.right = right;
	}

	public EAnaBinaryTree<E> getLeft() {
		return left;
	}

	public void setLeft(EAnaBinaryTree<E> left) {
		this.left = left;
	}

	public EAnaBinaryTree<E> getParent() {
		return parent;
	}

	public void setParent(EAnaBinaryTree<E> parent) {
		this.parent = parent;
	}
}
