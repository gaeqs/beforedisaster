package com.degoos.beforedisaster.tree;

import com.degoos.beforedisaster.EIterator;
import com.degoos.beforedisaster.list.ELinkedList;
import com.degoos.beforedisaster.list.EList;

import java.util.Iterator;

public class ESimpleLinkedTree {

	private int value;
	private EList<ESimpleLinkedTree> children;

	public ESimpleLinkedTree(int value){
		this.value=value;
		children = new ELinkedList<>();
	}

	public int getValue() {
		return value;
	}
	public boolean isLeaf(){
		return children.isEmpty();
	}
	public ESimpleLinkedTree dameElPutoArbol (int value){
		if(this.value==value) return this;
		EIterator<ESimpleLinkedTree> iterador = children.iterator();
		ESimpleLinkedTree eskere =null;

		while(eskere==null&&iterador.hasNext()){
			eskere = iterador.next().dameElPutoArbol(value);
		}
		return eskere;


	}
}
