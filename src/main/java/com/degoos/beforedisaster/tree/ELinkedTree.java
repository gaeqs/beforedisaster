package com.degoos.beforedisaster.tree;

import com.degoos.beforedisaster.EIterator;
import com.degoos.beforedisaster.list.EList;

public class ELinkedTree {

	private ELinkedTreeNode node;

	public ELinkedTree() {
		node = null;
	}

	public ELinkedTree(int value) {
		node = new ELinkedTreeNode(value);
	}

	public boolean isEmpty() {
		return node == null;
	}

	public boolean isLeaf() {
		if (node == null) throw new IllegalStateException("The tree is empty.");
		EList<ELinkedTree> children = node.getChildren();
		if (children.isEmpty()) return true;
		EIterator<ELinkedTree> iterator = children.iterator();
		while (iterator.hasNext()) {
			if (!iterator.next().isEmpty()) return false;
		}
		return true;
	}

	public ELinkedTree getSubtree(int value) {
		if (node == null) throw new IllegalStateException("The tree is empty");
		if (node.getValue() == value) return this;

		ELinkedTree data = null;
		ELinkedTree current;
		EIterator<ELinkedTree> iterator = node.getChildren().iterator();
		while (data == null && iterator.hasNext()) {
			current = iterator.next();
			if (!current.isEmpty())
				data = current.getSubtree(value);
		}
		return data;
	}


}
