package com.degoos.beforedisaster.tree;

import com.degoos.beforedisaster.list.EArrayList;
import com.degoos.beforedisaster.list.EList;

public class ELinkedTreeNode {

	private EList<ELinkedTree> children;
	private int value;

	public ELinkedTreeNode(int value) {
		children = new EArrayList<>();
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public EList<ELinkedTree> getChildren() {
		return children;
	}
}
