package com.degoos.beforedisaster.set;

import com.degoos.beforedisaster.EIterator;

public class EArraySet<E> implements ESet<E> {

	private static final int DEFAULT_SIZE = 10;

	private E[] array;
	private int size;

	public EArraySet() {
		array = (E[]) new Object[DEFAULT_SIZE];
		size = 0;
	}

	public EArraySet(int defaultSize) {
		array = (E[]) new Object[defaultSize];
		size = 0;
	}


	@Override
	public boolean add(E elem) {
		if (contains(elem)) return false;
		if (size == array.length) expand();
		array[size] = elem;
		size++;
		return true;
	}

	@Override
	public boolean remove(E elem) {
		boolean found = false;
		int current = 0;
		while (!found && current < size) {
			if (array[current].equals(elem)) {
				found = true;

				for (int i = current; i < size - 1; i++)
					array[i] = array[i + 1];
				array[size - 1] = null;
				size--;

			} else current++;
		}
		return found;
	}

	@Override
	public boolean contains(E elem) {
		boolean found = false;
		int current = 0;
		while (!found && current < size) {
			if (array[current].equals(elem))
				found = true;
			else current++;
		}
		return found;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void clear() {
		for (int i = 0; i < size; i++)
			array[i] = null;
		size = 0;
	}

	@Override
	public int addAll(ESet<E> set) {
		EIterator<E> iterator = set.iterator();
		int added = 0;
		E current;
		while (iterator.hasNext()) {
			current = iterator.next();
			if (!contains(current)) {
				add(current);
				added++;
			}
		}
		return added;
	}

	@Override
	public int removeAll(ESet<E> set) {
		EIterator<E> iterator = set.iterator();
		int removed = 0;
		E current;
		while (iterator.hasNext()) {
			current = iterator.next();
			if (contains(current)) {
				remove(current);
				removed++;
			}
		}
		return removed;
	}

	@Override
	public int removeExcept(ESet<E> set) {
		int removed = 0;
		for (int current = size - 1; current >= 0; current--) {
			if (!set.contains(array[current])) {
				for (int i = current; i < size - 1; i++)
					array[i] = array[i + 1];
				array[size - 1] = null;
				size--;
				removed++;
			}
		}
		return removed;
	}

	@Override
	public EIterator<E> iterator() {
		return new EArraySetIterator();
	}

	private void expand() {
		E[] newArray = (E[]) new Object[array.length * 2];
		for (int i = 0; i < size; i++)
			newArray[i] = array[i];
		array = newArray;
	}

	@Override
	public String toString() {
		if(size == 0) return "{}";
		String string = "{";
		for (int i = 0; i < size - 1; i++)
			string += array[i] + ", ";
		return string + array[size - 1] + "}";
	}

	private class EArraySetIterator implements EIterator<E> {

		private int current;

		public EArraySetIterator() {
			current = 0;
		}

		@Override
		public boolean hasNext() {
			return current < size;
		}

		@Override
		public E next() {
			E elem = array[current];
			current++;
			return elem;
		}
	}

}
