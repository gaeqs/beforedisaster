package com.degoos.beforedisaster.set;

import com.degoos.beforedisaster.EIterator;

public interface ESet<E> {

	boolean add(E elem);

	boolean remove (E elem);

	boolean contains (E elem);

	int size ();

	boolean isEmpty ();

	void clear ();

	int addAll (ESet<E> set);

	int removeAll (ESet<E> set);

	int removeExcept (ESet<E> set);

	EIterator<E> iterator();
}
